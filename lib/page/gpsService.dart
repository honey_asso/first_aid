import 'package:first_aid/widget/NavigationDrawerWidget.dart';
import 'package:flutter/material.dart';

class GPSService extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
    drawer: NavigationDrawerWidget(),
    appBar: AppBar(
      title: Text('People'),
      centerTitle: true,
      backgroundColor: Colors.green, 
    ),
  );
}