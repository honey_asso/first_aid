
import 'package:flutter/material.dart';
import 'package:first_aid/page/gpsService.dart';
import 'package:first_aid/page/language_page.dart';

class NavigationDrawerWidget extends StatelessWidget {
  final padding = EdgeInsets.symmetric(horizontal: 20);

  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: Material(
      color: Colors.red,
      child: ListView(children: <Widget>[
        DrawerHeader( 
          decoration: BoxDecoration(
            color: Colors.white,
          ),
          child: Stack(
            children: <Widget>[
              Align(
                alignment: Alignment.centerLeft,
                child: CircleAvatar(
                  backgroundImage: AssetImage('assets/images/firstaid.png'),
                  radius: 50.0,
                ),
              ),
              Align(
                alignment: Alignment.centerRight,
                child: Text(
                  'First Aid Guidance',
                  style: TextStyle(
                      color: Colors.red,
                      fontSize: 20.0,
                      fontWeight: FontWeight.w600),
                ),
              ),
            ],
          ),
        ),

        const SizedBox(height: 16),
        buildMenuItem(
          text: 'Language',
          icon: Icons.language,
          onClicked: () => selectedItem(context, 0),
        ),
        const SizedBox(height: 16),
        buildMenuItem(
          text: 'GPS Service',
          icon: Icons.gps_fixed,
          onClicked: () => selectedItem(context, 1),
        ),
        const SizedBox(height: 16),
        buildMenuItem(
          text: 'Contacts',
          icon: Icons.contact_page_outlined,
          onClicked: () => selectedItem(context, 2),
        ),
        const SizedBox(height: 16),
        buildMenuItem(
          text: 'Emergency Lines',
          icon: Icons.phone,
          onClicked: () => selectedItem(context, 3),
        ),
        //DIVIDER LINE
        const SizedBox(height: 24),
        Divider(color: Colors.white70),
        const SizedBox(height: 24),
        buildMenuItem(
          text: 'First Aid Websites',
          icon: Icons.wifi,
          onClicked: () => selectedItem(context, 4),
        ),
        const SizedBox(height: 16),
        buildMenuItem(
          text: 'Help',
          icon: Icons.help,
          onClicked: () => selectedItem(context, 5),
        ),
      ]),
    ));
  }

  Widget buildMenuItem({
    required String text,
    required IconData icon,
    VoidCallback? onClicked,
  }) {
    final color = Colors.white;
    final hoverColor = Colors.white70;

    return ListTile(
      leading: Icon(icon, color: color),
      title: Text(text,
          style: TextStyle(color: color, fontWeight: FontWeight.w500)),
      hoverColor: hoverColor,
      onTap: onClicked,
    );
  }

  void selectedItem(BuildContext context, int index) {
    Navigator.of(context).pop();
    switch (index) {
      case 0:
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => LanguagePage(),
        ));
        break;

      case 1:
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => GPSService(),
        ));
        break;
    }
  }
}
