import 'package:flutter/material.dart';

class Circulatory extends StatelessWidget {
  void selectedItem(BuildContext ctx, int index) {
    // Navigator.of(ctx).pop();
    switch (index) {
      case 0:
        Navigator.of(ctx).push(
          MaterialPageRoute(
            builder: (_) {
              return Circulatory();
            },
          ),
        );
        break;
    }
  }

  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text('Respiratory Complications'),
          centerTitle: true,
          backgroundColor: Colors.red,
        ),
        body: ListView(
          padding: const EdgeInsets.all(8),
          children: <Widget>[
            Container(
                height: 80,
                child: InkWell(
                  onTap: () => selectedItem(context, 0),
                  child: Card(
                    //margin: EdgeInsets.all(6),
                    elevation: 8.0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(6.0),
                    ),
                    child: Row(children: <Widget>[
                      Container(
                        height: 100,
                        padding: EdgeInsets.all(15),
                        child: CircleAvatar(
                          backgroundImage:
                              AssetImage('assets/images/heartattack.png'),
                          radius: 30.0,
                        ),
                      ),
                      Container(
                        alignment: Alignment.center,
                        height: 50,
                        //padding: EdgeInsets.all(10),
                        child: Text(
                          'Heart Attack',
                          style: TextStyle(color: Colors.black, fontSize: 18.0, fontWeight: FontWeight.w500),
                        ),
                      ),
                    ]),
                  ),
                )),
                Container(
                height: 80,
                child: InkWell(
                  onTap: () => selectedItem(context, 1),
                  child: Card(
                    //margin: EdgeInsets.all(6),
                    elevation: 8.0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(6.0),
                    ),
                    child: Row(children: <Widget>[
                      Container(
                        height: 100,
                        padding: EdgeInsets.all(15),
                        child: CircleAvatar(
                          backgroundImage:
                              AssetImage('assets/images/angina.jpeg'),
                          radius: 30.0,
                        ),
                      ),
                      Container(
                        alignment: Alignment.center,
                        height: 50,
                        //padding: EdgeInsets.all(10),
                        child: Text(
                          'Angina Pectoris',
                          style: TextStyle(color: Colors.black, fontSize: 18.0, fontWeight: FontWeight.w500),
                        ),
                      ),
                    ]),
                  ),
                )),
                Container(
                height: 80,
                child: InkWell(
                  onTap: () => selectedItem(context, 2),
                  child: Card(
                    //margin: EdgeInsets.all(6),
                    elevation: 8.0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(6.0),
                    ),
                    child: Row(children: <Widget>[
                      Container(
                        height: 100,
                        padding: EdgeInsets.all(15),
                        child: CircleAvatar(
                          backgroundImage:
                              AssetImage('assets/images/faint.jpeg'),
                          radius: 30.0,
                        ),
                      ),
                      Container(
                        alignment: Alignment.center,
                        height: 50,
                        //padding: EdgeInsets.all(10),
                        child: Text(
                          'Fainting',
                          style: TextStyle(color: Colors.black, fontSize: 18.0, fontWeight: FontWeight.w500),
                        ),
                      ),
                    ]),
                  ),
                )),
                Container(
                height: 80,
                child: InkWell(
                  onTap: () => selectedItem(context, 3),
                  child: Card(
                    //margin: EdgeInsets.all(6),
                    elevation: 8.0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(6.0),
                    ),
                    child: Row(children: <Widget>[
                      Container(
                        height: 100,
                        padding: EdgeInsets.all(15),
                        child: CircleAvatar(
                          backgroundImage:
                              AssetImage('assets/images/shocked.jpeg'),
                          radius: 30.0,
                        ),
                      ),
                      Container(
                        alignment: Alignment.center,
                        height: 50,
                        //padding: EdgeInsets.all(10),
                        child: Text(
                          'Shock',
                          style: TextStyle(color: Colors.black, fontSize: 18.0, fontWeight: FontWeight.w500),
                        ),
                      ),
                    ]),
                  ),
                )),
          ],
        ),
      );
}
