import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:first_aid/widget/respiratoryGuidance.dart';
import 'package:first_aid/widget/circulatoryGuidance.dart';

class HomeScreen extends StatelessWidget {
  void selectedItem(BuildContext ctx, int index) {
    // Navigator.of(ctx).pop();
    switch (index) {
      case 0:
        Navigator.of(ctx).push(
          MaterialPageRoute(
            builder: (_) {
              return Respiratory();
            },
          ),
        );
        break;
        case 1:
        Navigator.of(ctx).push(
          MaterialPageRoute(
            builder: (_) {
              return Circulatory();
            },
          ),
        );
        break;
    }

    // Navigator.of(ctx).push(MaterialPageRoute(builder: (_) {
    //   return NewScreen();
    // },),);
  }

  @override
  Widget build(BuildContext context) {
    //var size = MediaQuery.of(context).size;
    
    return InkWell(
      // onTap: () => selectCategory(context),
      child: Stack(
        children: <Widget>[
          //Expanded(child:
          GridView.count(
            padding: EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 16),
            mainAxisSpacing: 18,
            crossAxisSpacing: 18,
            primary: false,
            crossAxisCount: 2,
            children: <Widget>[
              Card(
                  margin: EdgeInsets.all(8),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(16)),
                  elevation: 4,
                  child: InkWell(
                    onTap: () => selectedItem(context, 0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Align(
                          alignment: Alignment.center,
                          child: CircleAvatar(
                            backgroundImage:
                                AssetImage('assets/images/respiratory.jpeg'),
                            radius: 65.0,
                          ),
                        ),
                        //SvgPicture.asset('assets/images/blackfirst.svg', height: 80,),
                        Text(
                          'Respiratory',
                          style: TextStyle(fontSize: 16, color: Colors.black, fontWeight: FontWeight.w500),
                        )
                      ],
                    ),
                  )),
              Card(
                  margin: EdgeInsets.all(8),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(16)),
                  elevation: 4,
                  child: InkWell(
                    onTap: () => selectedItem(context, 1),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Align(
                          alignment: Alignment.center,
                          child: CircleAvatar(
                            backgroundImage:
                                AssetImage('assets/images/circulatory.jpeg'),
                            radius: 65.0,
                          ),
                        ),
                        //SvgPicture.asset('assets/images/blackfirst.svg', height: 80,),
                        Text(
                          'Circulatory',
                          style: TextStyle(fontSize: 16, color: Colors.black, fontWeight: FontWeight.w500),
                        )
                      ],
                    ),
                  )),
              Card(
                  margin: EdgeInsets.all(8),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(16)),
                  elevation: 4,
                  child: InkWell(
                    onTap: () => selectedItem(context, 2),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Align(
                          alignment: Alignment.center,
                          child: CircleAvatar(
                            backgroundImage:
                                AssetImage('assets/images/bleed.jpeg'),
                            radius: 65.0,
                          ),
                        ),
                        //SvgPicture.asset('assets/images/blackfirst.svg', height: 80,),
                        Text(
                          'Bleeding',
                          style: TextStyle(fontSize: 16, color: Colors.black, fontWeight: FontWeight.w500),
                        )
                      ],
                    ),
                  )),
              Card(
                  margin: EdgeInsets.all(8),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(16)),
                  elevation: 4,
                  child: InkWell(
                    onTap: () => selectedItem(context, 3),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Align(
                          alignment: Alignment.center,
                          child: CircleAvatar(
                            backgroundImage:
                                AssetImage('assets/images/cpr.jpeg'),
                            radius: 65.0,
                          ),
                        ),
                        //SvgPicture.asset('assets/images/blackfirst.svg', height: 80,),
                        Text(
                          'CPR',
                          style: TextStyle(fontSize: 16, color: Colors.black, fontWeight: FontWeight.w500),
                        )
                      ],
                    ),
                  )),
              Card(
                  margin: EdgeInsets.all(8),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(16)),
                  elevation: 4,
                  child: InkWell(
                    onTap: () => selectedItem(context, 4),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Align(
                          alignment: Alignment.center,
                          child: CircleAvatar(
                            backgroundImage:
                                AssetImage('assets/images/car.png'),
                            radius: 65.0,
                          ),
                        ),
                        //SvgPicture.asset('assets/images/blackfirst.svg', height: 80,),
                        Text(
                          'Car Accident',
                          style: TextStyle(fontSize: 16, color: Colors.black, fontWeight: FontWeight.w500),
                        )
                      ],
                    ),
                  )),
              Card(
                  margin: EdgeInsets.all(8),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(16)),
                  elevation: 4,
                  child: InkWell(
                    onTap: () => selectedItem(context, 5),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Align(
                          alignment: Alignment.center,
                          child: CircleAvatar(
                            backgroundImage:
                                AssetImage('assets/images/stingsbites.jpeg'),
                            radius: 65.0,
                          ),
                        ),
                        //SvgPicture.asset('assets/images/blackfirst.svg', height: 80,),
                        Text(
                          'Stings and Bites',
                          style: TextStyle(fontSize: 16, color: Colors.black, fontWeight: FontWeight.w500),
                        )
                      ],
                    ),
                  )),
              Card(
                  margin: EdgeInsets.all(8),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(16)),
                  elevation: 4,
                  child: InkWell(
                    onTap: () => selectedItem(context, 6),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Align(
                          alignment: Alignment.center,
                          child: CircleAvatar(
                            backgroundImage:
                                AssetImage('assets/images/poison.png'),
                            radius: 65.0,
                          ),
                        ),
                        //SvgPicture.asset('assets/images/blackfirst.svg', height: 80,),
                        Text(
                          'Poison',
                          style: TextStyle(fontSize: 16, color: Colors.black, fontWeight: FontWeight.w500),
                        )
                      ],
                    ),
                  )),
              Card(
                  margin: EdgeInsets.all(8),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(16)),
                  elevation: 4,
                  child: InkWell(
                    onTap: () => selectedItem(context, 7),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Align(
                          alignment: Alignment.center,
                          child: CircleAvatar(
                            backgroundImage:
                                AssetImage('assets/images/heat.jpeg'),
                            radius: 65.0,
                          ),
                        ),
                        //SvgPicture.asset('assets/images/blackfirst.svg', height: 80,),
                        Text(
                          'Heat and Cold',
                          style: TextStyle(fontSize: 16, color: Colors.black, fontWeight: FontWeight.w500),
                        )
                      ],
                    ),
                  )),
            ],
          ),
          //)
        ],
      ),
    );
  }
}
