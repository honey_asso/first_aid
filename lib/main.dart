import 'package:first_aid/localization/demo_localization.dart';
import 'package:flutter/material.dart';
import 'package:first_aid/widget/NavigationDrawerWidget.dart';
import 'package:first_aid/widget/guidanceCategory.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'classes/language.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  static final String title = 'First Aid Guiadance';
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'First Aid Guiadance',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      supportedLocales: [Locale('en', 'US'), Locale('am', 'ET')],
      localizationsDelegates: [
        DemoLocalization.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      localeResolutionCallback: (deviceLocale, supportedLocales){
        for(var locale in supportedLocales){
          if(locale.languageCode == deviceLocale.languageCode && locale.countryCode == deviceLocale.countryCode){
            return deviceLocale;
          }
        }
        return supportedLocales.first;
      },
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);
  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  void _changeLanguage(Language language) {
    print(language.languageCode);
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        drawer: NavigationDrawerWidget(),
        body: HomeScreen(),
        appBar: AppBar(
          title: Text(MyApp.title),
          actions: <Widget>[
            Padding(
              padding: EdgeInsets.all(8.0),
              child: DropdownButton(
                onChanged: (Language language) {
                  _changeLanguage(language);
                },
                underline: SizedBox(),
                icon: Icon(
                  Icons.language,
                  color: Colors.white,
                ),
                items: Language.languageList()
                    .map<DropdownMenuItem<Language>>((lang) => DropdownMenuItem(
                          value: lang,
                          child: Row(
                            children: <Widget>[
                              Text(lang.flag),
                              Text(lang.name)
                            ],
                          ),
                        ))
                    .toList(),
              ),
            )
          ],
        ),

        // INSERT MAIN PAGE HERE!!!
        // body: Builder(
        //   builder: (context) => Container(
        //     padding: EdgeInsets.all(20),
        //     color: Colors.white70,
        //     child: GridTile(
        //       child: Text('First', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 48), textAlign: TextAlign.center,),

        //     ),
        // alignment: Alignment.center,
        // padding: EdgeInsets.symmetric(horizontal:32),
        // child: ButtonWidget(
        //   icon: Icons.open_in_new,
        //   text: 'Open Drawer',
        //   onClicked: (){
        //     Scaffold.of(context).openDrawer();
        //     //Scaffold.of(context).openEndDrawer();
        //   },
        // ),
        // ),
        // ),
      );
}
